#include "array.h"
#include <stdio.h>
CS225::Array::Array(int * array,
	unsigned int _size,
	const ElementFactory* _pElementFactory)
	: data(new AbstractElement*[_size]), size(_size),
	pElementFactory(_pElementFactory), counter(new int(1))
	{
	for (unsigned int i=0;i<size;++i) 
	{
		//create Element with id = 1 (that is Element1 )
		data[i] = pElementFactory->MakeElement( 1, array[i] );	
	}
}

int CS225::Array::Get(unsigned int pos) const { return data[pos]->Get(); }

void CS225::Array::Set(int id, int pos, int value)
{ 
	//setter will delete the old and create new 
	//slightly inefficient if new and old have the same type
	if(*this->counter==1)
	{ 
		delete data[pos];
		data[pos] = pElementFactory->MakeElement(id,value); 
	}
	else
	{
		AbstractElement**temp = data;
		data = new AbstractElement*[size];
		for (unsigned int i = 0; i < this->size; i++)
		{
			data[i] = temp[i]->lasun();
		}
		(*counter)--;
		this->counter = new int(1);

		delete data[pos];
		data[pos] = pElementFactory->MakeElement(id, value);
		
	}
}

void CS225::Array::Print() const {
	for (unsigned int i=0;i<size;++i) data[i]->Print(); 
	std::cout << std::endl;
}

//copy constructor
CS225::Array::Array(const Array& rhs) : data(rhs.data),size(rhs.size), pElementFactory(rhs.pElementFactory), counter(rhs.counter)
{
	/*pElementFactory = rhs.pElementFactory;
	counter = rhs.counter;
	data = rhs.data;
	size = rhs.size;*/
		(*counter)++;
	
}
//normal assignment operator =

CS225::Array& CS225::Array::operator=(const Array& rhs)
{
	
	if (this == &rhs)
		return *this;
	
	if (*counter == 1)
	{	
		for (unsigned int i = 0; i < size; ++i)
		{
			
				delete *(data + i);
		}
		delete counter;
		delete[] data;
	}
	else 
	{
		
		(*counter)--;
	}
	
	pElementFactory = rhs.pElementFactory;
	counter = rhs.counter;
	
	data = rhs.data;
	(*counter)++;
	size = rhs.size;
	return *this;
}
/*
CS225::Array& CS225::Array::operator=(const Array& rhs)
{

	if (this == &rhs)
		return *this;

	if (*counter == 1)
	{
		for (unsigned int i = 0; i < size; ++i)
		{
			delete *(data + i);
			
		}
		delete[] data;
		delete counter;
	}
	
	(*counter)--;

	//delete counter;

	pElementFactory = rhs.pElementFactory;
	counter = rhs.counter;
	data = rhs.data;
	(*counter)++;
	size = rhs.size;
	return *this;
}

*/

CS225::Array::~Array()
{

	if (*counter == 1)
	{
		for (unsigned int i = 0; i < size; ++i)
		{
		
			delete *(data + i);
		}

			delete[] data;
			
			delete counter;		
	
	}
	else
	{
		(*counter)--;
	}
}