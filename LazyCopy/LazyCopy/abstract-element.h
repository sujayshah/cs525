#pragma once
#define ABSTRACT_ELEMENT_H
#include "CRTP.h"
namespace CS225
{
	class AbstractElement : public ObjectCounter<AbstractElement>
	{
		public:
			AbstractElement() {};
			virtual AbstractElement* lasun()=0;
			virtual ~AbstractElement() {};
			virtual int Get() const=0;
			virtual void Set(int new_val)=0;
			virtual void Print() const=0;

			
	};
}