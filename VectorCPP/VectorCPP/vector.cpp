#include "vector.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include<cstddef>
//using  namespace std;
using  namespace CS225;
ElementNode *current;
namespace CS225
{
	std::ostream& operator<<(std::ostream &out, const SparseVector &v)
	{
		int i, last_pos = -1;
		ElementNode* p_e = v.pHead;
		while (p_e) {
			for (i = last_pos + 1; i<p_e->pos; ++i) out << " " << "0";
			out << " " << p_e->data;
			last_pos = p_e->pos;
			p_e = p_e->next;
		}
		for (i = last_pos + 1; i<v.dimension; ++i) out << " " << "0";

		return out;
	}
	SparseVector operator*(int m, const SparseVector &v)
	{
		ElementNode *current;
		SparseVector result;
		int data = 0;
		current = v.pHead;
		while (current != NULL)
		{

			data = current->data * m;
			result.Insert(data, current->pos);
			current = current->next;
		}
		return result;
	}

}
// destructor
SparseVector::~SparseVector()
{
	while (pHead != NULL)
	{
		current = pHead->next;
		/*delete head pointer*/
		pHead->next = NULL;
		delete(pHead);
		pHead = current;
		if (current == NULL)
			return;
		current = current->next;
	}
}

int SparseVector::Get(long pos) const
{
	ElementNode *current;
	current = pHead;
	while (current != NULL)
	{
		if (current->pos == pos)
			return current->data;
		//given pos DNE
		else if (current->pos > pos)
			return 0;

		current = current->next;
	}
	// if no pos found
	return 0;
}
void SparseVector::PrintRaw() const
{
	ElementNode* curr = pHead;
	std::cout << "Raw vector: ";
	while (curr)
	{
		std::cout << "(" << curr->data << ", " << curr->pos << ")";
		curr = curr->next;
	}
	std::cout << std::endl;
}

void SparseVector::Insert(int data, long pos)
{
	ElementNode *new_node, *prev;
	current = pHead;
	prev = NULL;
	do
	{
		/*Initial element insertion*/
		if (pHead == NULL)
		{
			if (data == 0)
				return;
			new_node = new ElementNode();

			if (new_node == NULL)
			{
				std::cout << "Error in creating new node\n";
				return;
			}
			/* assign head */
			pHead = new_node;
			new_node->data = data;
			new_node->pos = pos;
			new_node->next = NULL;
			dimension = pos + 1;
			return;
		}
		/*insertion at existing position */
		else if (current->pos == pos)
		{
			if (data == 0)
			{
				Delete(current->pos);
				return;
			}
			/*if new pos is at existing head*/
			if (current == pHead)
			{
				current->data = data;
				return;
			}
			current->data = data;
			return;
		}
		else if (pos<current->pos)
		{
			if (data == 0)
			{
				Delete(current->pos);
				return;
			}

			/*if new pos is less than head pos*/
			if (current == pHead)
			{
				new_node = new ElementNode();
				if (new_node == NULL)
				{
					////perror("Error in creating new node\n");
					return;
				}
				new_node->data = data;
				new_node->pos = pos;
				new_node->next = current;
				pHead = new_node;
				dimension = pos + 1;
				return;
			}
			else
			{	/*insert in middle*/
				new_node = new ElementNode();
				if (new_node == NULL)
				{
					////perror("Error in creating new node\n");
					return;
				}
				new_node->data = data;
				new_node->pos = pos;
				prev->next = new_node;
				new_node->next = current;
				dimension = pos + 1;
				return;
			}
		}
		/*keep iterating till end*/
		prev = current;
		current = current->next;
	} while (current != NULL);

	/*if no existing position found create new node and store value at that position*/
	if (current == NULL)
	{
		/*if current points to head*/
		if (current == pHead)
		{
			if (data == 0)
				return;
			new_node = new ElementNode();
			if (new_node == NULL)
			{
				////perror("Error in creating new node\n");
				return;
			}

			new_node->next = current;
			prev->next = new_node;
			new_node->data = data;
			new_node->pos = pos;
			dimension = pos + 1;
			return;
		}
		else
		{
			if (data == 0)
				return;
			/*if new pos is in the end of the list*/
			new_node = new ElementNode();
			if (new_node == NULL)
			{
				////perror("Error in creating new node\n");
				return;
			}
			prev->next = new_node;
			new_node->next = current;
			new_node->data = data;
			new_node->pos = pos;
			dimension = pos + 1;
			return;
		}
	}

}

void SparseVector::Delete(long pos)
{
	ElementNode *prev;
	if (pHead == NULL)
	{
		//perror("Node doesn't exist\n");
		return;
	}
	/*reposition current in begining*/
	current = pHead;
	do
	{
		/*if delete 1st element*/
		if (current == pHead && current->pos == pos)
		{
			current = current->next;
			delete(pHead);
			/*assign new head*/
			pHead = current;
			return;
		}


		prev = current;
		current = current->next;

	} while (current->pos != pos);

	if (current->pos == pos)
	{
		/*if (current == pHead)
		{

		}*/
		prev->next = current->next;
		delete(current);
		return;
	}
	else
	{
		//printf("No node found at %d position\n",pos);
		return;
	}
}

//for writing
ElementProxy SparseVector::operator[](long pos)
{
	ElementProxy Data(*this, pos);
	return Data;
}
//for reading
int CS225::SparseVector::operator[](long pos) const
{
	return this->Get(pos);
}
ElementProxy::ElementProxy(SparseVector &v, long pos) :v(v), pos(pos) {}


ElementProxy& ElementProxy::operator=(int data)
{
	v.Insert(data, this->pos);
	return *this;
}

SparseVector& SparseVector:: operator=(const SparseVector &v)
{
	//SparseVector result;
	pHead = v.pHead;
	dimension = v.dimension;

	return *this;
}

SparseVector SparseVector:: operator+(const SparseVector &v)
{
	ElementNode  *e1, *e2;
	SparseVector result;
	e1 = this->pHead;
	e2 = v.pHead;
	while (e1 != NULL&&e2 != NULL)
	{
		if (e1->pos == e2->pos)
		{
			result.Insert(e1->data + e2->data, e1->pos);
			e1 = e1->next;
			e2 = e2->next;
		}
		else if (e1->pos < e2->pos)
		{

			result.Insert(e1->data, e1->pos);
			e1 = e1->next;
		}
		else
		{
			result.Insert(e2->data, e2->pos);
			e2 = e2->next;
		}
		if (e1 == NULL)
		{
			while (e2 != NULL)
			{
				result.Insert(e2->data, e2->pos);
				e2 = e2->next;
			}
		}
		else if (e2 == NULL)
		{
			while (e1 != NULL)
			{
				result.Insert(e1->data, e1->pos);
				e1 = e1->next;
			}
		}
	}
	return result;
}

int SparseVector:: operator*(const SparseVector &v)
{
	ElementNode *e1, *e2;
	int result = 0;
	e1 = this->pHead;
	e2 = v.pHead;
	while (e1 != NULL&&e2 != NULL)
	{
		if (e1->pos == e2->pos) {

			result += e1->data*e2->data;
			e1 = e1->next;
			e2 = e2->next;
		}
		else if (e1->pos < e2->pos)
		{
			e1 = e1->next;
		}
		else
		{
			e2 = e2->next;
		}
	}
	return result;
}
int CS225::SparseVector::operator*(const SparseVector & rhs) const
{
	ElementNode *e1, *e2;
	int result = 0;
	e1 = this->pHead;
	e2 = rhs.pHead;
	while (e1 != NULL&&e2 != NULL)
	{
		if (e1->pos == e2->pos) {

			result += e1->data*e2->data;
			e1 = e1->next;
			e2 = e2->next;
		}
		else if (e1->pos < e2->pos)
		{
			e1 = e1->next;
		}
		else
		{
			e2 = e2->next;
		}
	}
	return result;
}
SparseVector SparseVector:: operator*(int m)
{
	ElementNode *current;
	SparseVector result;
	int data = 0;
	current = pHead;
	while (current != NULL)
	{

		data = current->data * m;
		result.Insert(data, current->pos);
		current = current->next;
	}
	return result;
}

//Copy Constructor
SparseVector::SparseVector(const SparseVector&v1) :pHead(NULL), dimension(0)
{
	ElementNode *current1;
	current1 = v1.pHead;
	dimension = v1.dimension;
	if (v1.pHead != NULL)
	{
		while (current1 != NULL)
		{
			Insert(current1->data, current1->pos);
			current1 = current1->next;
		}
	}
}

CS225::ElementProxy::operator int() const
{
	return v.Get(pos);
}


ElementProxy& ElementProxy ::operator +=(int rhs)
{
	int data = 0;
	data = v.Get(this->pos);
	data += rhs;
	v.Insert(data, this->pos);
	return *this;
}

ElementProxy& ElementProxy ::operator -=(int rhs)
{
	int data = 0;
	data = v.Get(this->pos);
	data -= rhs;
	v.Insert(data, this->pos);
	return *this;
}

ElementProxy& ElementProxy ::operator =(ElementProxy &ep)
{
	int data;
	data = ep.v.Get(ep.pos);
	this->v.Insert(data, this->pos);
	return *this;
}
