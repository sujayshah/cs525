#include <iostream>
struct ElementNode
{
	int    data;
	int    pos;
	struct ElementNode* next;
};
namespace CS225
{
	class ElementProxy;

	class SparseVector
	{
	public:

		void Insert(int data, long pos);
		void Delete(long pos);
		int Get(long pos) const;
		void PrintRaw() const;

		friend std::ostream& operator<<(std::ostream &out, const SparseVector &v);
		friend SparseVector operator*(int m, const SparseVector &v);
		//Default constructor
		SparseVector() : pHead(NULL), dimension(0)
		{
		}
		~SparseVector();
		//Copy constructor
		SparseVector(const SparseVector &v1);
		ElementProxy operator[] (long pos);// for writing
		int operator[] (long pos)const; //for reading
		SparseVector& operator=(const SparseVector &v);
		SparseVector operator+(const SparseVector &v);
		SparseVector operator*(int m);
		int operator*(const SparseVector &v);
		int operator*(const SparseVector & rhs) const;
	private:
		ElementNode* pHead;
		long dimension;
	};

	class ElementProxy
	{
	public:
		ElementProxy(SparseVector &v, long pos);

		operator int() const;
		ElementProxy& operator =(int data);
		ElementProxy& operator +=(int rhs);
		ElementProxy& operator -=(int rhs);
		ElementProxy& operator =(ElementProxy &ep);

	private:
		SparseVector &v;
		long pos;
	};
	//std::ostream & operator<<(std::ostream & out, const SparseVector & v);
}
