#include "statistics.h"
#include <numeric> //accumulate
#include <cmath> //sqrt
#include <iterator> //ostream_iterator, back_inserter
#include <iomanip> //setfill setw
#include <iostream> //cout

Statistics::Statistics() : data() {}

std::ostream& operator<<( std::ostream& out, Statistics const& stat ) {
    std::copy( stat.data.begin(),  stat.data.end(),
               std::ostream_iterator<int>( out, " " ) );
    return out;
}

std::istream& operator>>( std::istream& in, Statistics & stat ) {
    std::copy( std::istream_iterator<int>(in),
               std::istream_iterator<int>(),
               std::back_inserter( stat.data ) );
    return in; 
}

float Statistics::Average()
{
	float avg = 0.0f,sum=0.0f;
	sum = std::accumulate(data.begin(),data.end(),0);
	avg = sum / data.size();
	return avg;
}
float Statistics::Deviation()
{
	float mean = this->Average();
	float deviation = std::accumulate(data.begin(), data.end(),);
}

class DeviationOperator
{
    float avg;
    DeviationOperator(float _avg):avg(_avg)
    {

    }
}