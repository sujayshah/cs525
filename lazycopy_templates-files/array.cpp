#include "array.h"
#include <iostream>

namespace CS225 
{


	template <typename DataType> 
	std::ostream& operator<<( std::ostream &out, ElementProxy<DataType> const& ep )
	{
                
                    out<<ep.array.data[ep.pos];
                             
                return out;
    }
    template<typename DataType>
    void Array<DataType>::Insert( int pos, DataType const& val ) {
        /*
         * ....
         */
		if (*shared_obj_count == 1)
		{
			//if obj isnt shared 
			if (pos >= size)
			{
				int old_size = size;
				size = 2 * pos + 1;
				DataType* new_data = new DataType[size];
				for (int i = 0; i < old_size; ++i)
				{
					new_data[i] = data[i];
				}

				delete[] data;
				data = new_data;
			}
			data[pos] = val;
		}
		else
		{
			//if obj is shared, perform deep copy then, modify
			(*shared_obj_count)--;
			shared_obj_count = new int(1);
			
			//copy old data
			DataType* temp = data;
			
			int old_size = size;
			data = new DataType[old_size];
			for (int i = 0; i < old_size; ++i)
			{
				data[i] = temp[i];
			}
		
			if (pos >= size)
			{
				size = 2 * pos + 1;
				DataType *new_data = new DataType[size];
			
				for (int i = 0; i < old_size; ++i)
				{
					new_data[i] = data[i];
				}
				//delete old data
				delete[] data;
				data = new_data;
			}
		

			//update val of specified position
			data[pos] = val;
		}
    }
	// constructor
	template<typename DataType>
 	Array<DataType>::Array():data(NULL),size(0),shared_obj_count(new int(1)){}
    
    //copy constructor
    template<typename DataType>
    Array<DataType>::Array(Array &arr):data(arr.data),size(arr.size),shared_obj_count(arr.shared_obj_count)
	{
		//shallow copy
		(*shared_obj_count)++;
	}
	
	//Destrutor
	template<typename DataType>
	Array<DataType>::~Array()
	{
		if(*shared_obj_count>1)
		{
			(*shared_obj_count)--;
		}
		else
		{
			delete[] data;
			delete shared_obj_count;
		}
    } 
	//overloaded operators-----------------------------------------------------------
	
	//for writing an element
     template <typename DataType>
     ElementProxy <DataType> Array<DataType>:: operator[](int position)
     {
        ElementProxy<DataType> Data(*this,position);
        return Data;
     }
	 //for reading an element
	 template <typename DataType>
     DataType& Array<DataType>:: operator[](int position)const
     {
        return data[position];
     }
	//assignment operator
	template<typename DataType>
	 Array<DataType>& Array<DataType>::operator=(Array<DataType>& array)
	{

		if(*shared_obj_count==1) {
			
			//owner can delete
			delete[] data;
			delete shared_obj_count;
		}
		else 
		{
			
			--(*shared_obj_count);
			//more than one owners//subsrtac ref count
			
		}
		
		

		data=array.data;
		size=array.size;
		shared_obj_count=array.shared_obj_count;
		(*shared_obj_count)++;
		return *this;
	}
	//insertion stream operator
    template<typename DataType>
    std::ostream& operator<<( std::ostream &out, Array<DataType> const& array ) {
        for ( int i=0; i<array.size; ++i ) {
            out << array.data[i] << " ";
        }
        return out;
    }
	
	template<typename DataType>
	  ElementProxy <DataType>& ElementProxy <DataType>::operator=(const DataType &val)
	{
		array.Insert(pos,val);
		return *this; 
	}
}
