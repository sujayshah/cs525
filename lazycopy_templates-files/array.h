#ifndef ARRAY_H
#define ARRAY_H
#include <iostream>

namespace CS225
{
	template <typename DataType> class Array;
	template <typename DataType>class ElementProxy;

	template <typename DataType>
	std::ostream& operator<<( std::ostream &out, Array<DataType> const& array );
	
	template <typename DataType>
	class Array
	{
		public:
		Array();
		~Array();
		Array(Array &arr);
	    ElementProxy<DataType> operator[](int position);
		DataType& operator[](int position)const;
		Array<DataType>& operator=(Array& array );
		void Insert( int pos, DataType const& val );
		friend std::ostream& operator<<<DataType>( std::ostream &out, Array<DataType> const& array );
		
		public:
		DataType *data;
		int size;
		int *shared_obj_count;
	
		
	};
	
	  
	
	template <typename DataType> 
	std::ostream& operator<<( std::ostream &out, ElementProxy<DataType> const& ep );
	
    template <typename DataType>
    class ElementProxy
    {
        public:
            ElementProxy(Array<DataType> &rhs, int position): array(rhs),pos(position)
            {
            }
            ElementProxy <DataType>& operator=(const DataType &val)
           ;
           friend std::ostream& operator<<<DataType>( std::ostream &out, ElementProxy<DataType> const& ep );
            
			
        public:
        Array<DataType> &array;
        int pos;
    };
	
	
}
#include "array.cpp" 
#endif
