#include <iostream>
using namespace std;

void bar(int code) {
	switch (code) {
	case 1: throw 1; /*throw an int*/ break;
	case 2: throw 1.0; /*throw a double*/ break;
	case 3: /*throw nothing*/ break;
	}
}
void foo(int code) {
	try { bar(code); }
	catch (double ex) {
		std::cout << "1"; //PRINT STATEMENT
	}
	std::cout << "2"; //PRINT STATEMENT
}
int main() {
	try { foo(3); /*1 or 2 or 3*/ }
	catch (int ex) {
		std::cout << "3"; //PRINT STATEMENT
	}
	std::cout << "4"; //PRINT STATEMENT
}