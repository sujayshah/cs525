#ifndef MEMFUN_ADAPTER_H
#define MEMFUN_ADAPTER_H
template <typename RetType, typename ClassType, typename ArgType>
class MFA_1arg 
{
		RetType (ClassType::*pointer) (ArgType);
	public:
		MFA_1arg( RetType (ClassType::*_pointer) (ArgType) ) : pointer(_pointer) {}
		
		RetType operator() ( ClassType& element,ArgType arg1) const
		 {
			return (element.*pointer)(arg1); //call
		}
	typedef  ClassType first_argument_type;
	typedef RetType result_type;
	typedef ArgType second_argument_type;
};


//helper functions
//for non-const member function
template <typename RetType, typename ClassType, typename ArgType>
MFA_1arg<RetType,ClassType,ArgType> mfp_1arg(RetType (ClassType::*pointer)( ArgType) ) 
{ 
	return MFA_1arg	 <RetType,ClassType,ArgType>(pointer); 
}

//const class
template <typename RetType, typename ClassType, typename ArgType>
 class MFA_1arg_const
 {
	RetType (ClassType::*pointer) (ArgType) const;
	public:
		MFA_1arg_const( RetType (ClassType::*_pointer) (ArgType) const ) : pointer(_pointer) {}
		
		RetType operator() ( const ClassType& element,const ArgType arg1) const
		 {
			return (element.*pointer)(arg1); //call
		}
	typedef ClassType first_argument_type;
	typedef RetType result_type;
	typedef ArgType second_argument_type;
 };

//for const member function
template <typename RetType, typename ClassType, typename ArgType>
MFA_1arg_const<RetType,ClassType,ArgType> mfp_1arg(RetType (ClassType::*pointer)( ArgType) const ) 
{ 
	return MFA_1arg_const <RetType,ClassType,ArgType>(pointer); 
}
#endif
