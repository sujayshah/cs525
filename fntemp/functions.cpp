#include <iostream>
#include "functions.h"

namespace CS225 {
    template <typename T>
    void display( T const *begin, T const *end )
    {
        if( begin != end ) std::cout << *begin++;
        while( begin<end ) {
            std::cout << ", " << *begin;
            ++begin;
        }
        std::cout << std::endl;
    }

    template <typename T>
    void swap( T *left, T *right )
    {
        T temp( *right );
        ( *right ) = ( *left );
        ( *left ) = temp;
    }

    template <typename T>
    void swap_ranges(T *begin,T* end, T *copy)
    {
        T temp;
       // int i=0;
        while(begin<end)
        {
            temp=*(begin);
            *(begin)=*(copy);
            *(copy)=temp;
			++begin;
			++copy;
        }
    }

    template <typename T>
    T* min_element( T *begin, T *end )
    {
        T* min=begin;
        while( begin!=end ) {
            if( *begin<*min ) min=begin;
            ++begin;
        }
        return min;
    }

    template <typename T>
    T const* min_element( T const *begin, T const *end )
    {
        T const* min=begin;
        while( begin!=end ) {
            if( *begin<*min ) min=begin;
            ++begin;
        }
        return min;
    }
	template <typename T>
	int count(T const *begin, T const *end, T item)
	{
		int count=0;
		while (begin < end)
		{
			if (*begin == item)
				count++;

			begin++;
		}
		return count;
	}
	//READ ONLY
	template <typename T>
	const int* find(const T *begin,const T *end, const T item)
	{
		while (begin < end)
		{
			if (*begin == item)
				return begin;

			begin++;
		}
		//no item found
		return end;
	}
	//READ & WRITE
	template <typename T>
	int* find(T *begin, T *end, const T item)
	{
		while (begin < end)
		{
			if (*begin == item)
				return begin;

			begin++;
		}
		//no item found
		return end;
	}

	template <typename T1, typename T2>
	T2* copy(const T1 *begin, const T1 *end, T2*copy_begin)
	{
		while (begin < end)
		{
			*copy_begin = *begin;
			begin++;
			copy_begin++;
		}
		return copy_begin;
	}

	template <typename T>
	void fill(T *begin, T *end, int val)
	{
		while (begin < end)
		{
			*begin = val;
			begin++;
		}
	}

	template <typename T>
	void replace(T *begin, T *end, int old_item, int new_item)
	{
		while (begin<end)
		{
			if (*begin == old_item)
			{
				*begin = new_item;
			}
			begin++;
		}
	}

	// READ ONLY
	template <typename T>
	const T* max_element(const T *begin, const T *end)
	{
		T const* max = begin;
		while (begin != end) {
			if (*begin>*max) max = begin;
			++begin;
		}
		return max;
	}

	// READ & WRITE
	template <typename T>
	T* max_element(T *begin, T *end)
	{
		T  *max = begin;
		while (begin != end) {
			if (*begin>*max) max = begin;
			++begin;
		}
		return max;
	}

	template <typename T1,typename T2>
	bool equal(T1 *begin, T1 *end, T2* cmp)
	{
		while (begin < end)
		{
			if (*begin == *cmp)
			{
				++begin;
				++cmp;
			}
			else
				return false;
		}
		return true;
	}

	template <typename T>
	T sum(T *begin, T*end)
	{
		T sum=0;
		while (begin < end)
		{
			sum += *begin;
			begin++;
		}
		return sum;
	}

	template <typename T>
	T* remove(T *begin, T*end, T item)
	{
		int count = 0,i=0;
		int size =  end-begin;
		T *new_end=end-1;
		T *temp;
		while (new_end >= begin)
		{
			if (*new_end == item)
			{
				--size;
				temp = new_end;
				for (i = 0; i < count; i++)
				{
					*temp = *(temp + 1);
					temp++;
				}
				--new_end;

			}
			else
			{
				--new_end;
			}
			++count;
		}
		// all items deleted
		if (size == 0)
			return begin;
		else // some or no items deleted
		return begin + size;
	}
}
