
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

class C {
	int i;
	public:
		C(int _i) : i(_i) {}
		void Square() { i*=i; }
		const int& Get() const { return i; }
};

//the functor that is able to call member pointer
template <typename RetType, typename ClassType>
class MFP {
		RetType (ClassType::*pointer) ();
	public:
		MFP( RetType (ClassType::*_pointer) () ) : pointer(_pointer) {}
		RetType operator() ( ClassType& element) {
			return (element.*pointer) (); //call
		}
};

//helper function
template <typename RetType, typename ClassType>
MFP<RetType,ClassType> mfp(RetType (ClassType::*pointer) () ) { 
	return MFP<RetType,ClassType>(pointer); 
}

int main () {
	std::vector<C> v;
	for (int i=0;i<10; ++i) v.push_back( C(i) );
	std::vector<C>::const_iterator it=v.begin(), it_end=v.end();
	for ( ; it!=it_end; ++it) { std::cout << it->Get() << " "; }
	std::cout << std::endl;

	std::for_each (v.begin(), v.end(), mfp( &C::Square ) );

	
	for ( it=v.begin(); it!=it_end; ++it) { std::cout << it->Get() << " "; }
	std::cout << std::endl;
    return 0;
}